-- Drop the Users table if it exists
DROP TABLE IF EXISTS users;

-- Create the Users table
CREATE TABLE users (
    id SERIAL,
    name VARCHAR(50),
    age INT
);

-- Create a test user
INSERT INTO users (name, age)
VALUES ('james', 22);
