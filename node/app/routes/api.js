const pg = require('pg');
const express = require('express');
const router = express.Router();

// middleware for every request
router.use(function(req, res, next) {
    next();
});

router.get('/', (req, res) => {
    res.send('PostgreSQL');
});

// routes
router.use('/users', require('./users'));

module.exports = router;
